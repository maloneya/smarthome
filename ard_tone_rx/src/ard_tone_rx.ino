/* Allows two Arduinos to send messages to each other over SoftwareSerial
 * 
 * Reference:
 * https://www.arduino.cc/en/Reference.SoftwareSerial
 * 
 * Wiring for two Arduinos:
 *    RX1 <-> TX2
 *    TX1 <-> RX2
 *    GND <-> GND
 *    
 *    Note: connect any pair of ground pins between the two Arduinos
 */
#include <SoftwareSerial.h>  
#include "pitches.h"

void playTone(); 

int myTxPin = 2;  // Chosen pin for transmitting data
int myRxPin = 3;  // Chosen pin for receiving data

int clockIntPin = 4; //sends interupts to the other arduino
int speakerpin = 8;
int lights = 9;

int light1 = 5;
int light2 = 6;
int light3 = 7;
int light4 = 9;
int light5 = 10;

// notes in the melody:
int melody[] = {
  NOTE_C4, NOTE_G3, NOTE_G3, NOTE_A3, NOTE_G3, 0, NOTE_B3, NOTE_C4
};

int melody3[] = {
  NOTE_C4, NOTE_G3, NOTE_E3, NOTE_A4, NOTE_B4, NOTE_A4, NOTE_G3, 0
};

int melody4[] = {
  NOTE_C4, 
  NOTE_G3, 
  NOTE_E3, 
  NOTE_A4, 
  NOTE_B4, 
  NOTE_AS4, 
  NOTE_A4, 
  NOTE_G3, 
  NOTE_E4, 
  NOTE_G4, 
  NOTE_A4, 
  NOTE_F4, 
  NOTE_G4, 
  NOTE_E4, 
  NOTE_C4, 
  NOTE_D4, 
  NOTE_B4
};

int noteDurations4[] = {
  4, 4, 4, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 4
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4, 8, 8, 4, 4, 4, 4, 4
};


// Create software serial object
SoftwareSerial softSerial(myRxPin, myTxPin);

void setup(){
  Serial.begin(9600);                                         // Begin the serial monitor at 9600 to write to the pi
  softSerial.begin(9600);                                     // Start the software serial at 9600 to read from the other arduino

  pinMode(clockIntPin, OUTPUT);
  pinMode(speakerpin, OUTPUT);

  pinMode(light1, OUTPUT);
  pinMode(light2, OUTPUT);
  pinMode(light3, OUTPUT);
  pinMode(light4, OUTPUT);
  pinMode(light5, OUTPUT);
  
  pinMode(motion, INPUT);
}

void loop(){
  //check for reqeusts from pi 
  if (Serial.available()) {
    byte incomingByte = Serial.read();
    Serial.println(String(incomingByte));
    //if the pi is asking for the time
    if (String(incomingByte) == String(99)) {                 // If character equals 'c'
      digitalWrite(clockIntPin, HIGH);       
      digitalWrite(clockIntPin, LOW);  //tell the other arduino to send us the time
    }
    //if it is asking us to play a tone
    else if (String(incomingByte) == String(109)) {           // If character equals 'm'
      playTone();
    }
    //if it is asking us to turn on the lights
    else if (String(incomingByte) == String(48)) {           // If character equals '0'
      turn0LED();
    }

    else if (String(incomingByte) == String(49)) {           // If character equals '1'
      turn1LED();
    }

    else if (String(incomingByte) == String(50)) {           // If character equals '2'
      turn2LED();
    }

    else if (String(incomingByte) == String(51)) {           // If character equals '3'
      turn3LED();
    }

    else if (String(incomingByte) == String(52)) {           // If character equals '4'
      turn4LED();
    }

    else if (String(incomingByte) == String(53)) {           // If character equals '5'
      turn5LED();
    }
  }

  //If this Arduino has received any data on the RX pin
  if(softSerial.available()){ 
	  String line = "";
	  while(softSerial.available()){
		line = line + (char)softSerial.read();
	  }
	  Serial.println(line);
  }
  
}


void playTone() {
  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < 8; thisNote++) {

    // to calculate the note duration, take one second
    // divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(8, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(8);
  }
}

void turn0LED(){
  digitalWrite(light1, LOW);
  digitalWrite(light2, LOW);
  digitalWrite(light3, LOW);
  digitalWrite(light4, LOW);
  digitalWrite(light5, LOW);
}

void turn1LED(){
  digitalWrite(light1, HIGH);
  digitalWrite(light2, LOW);
  digitalWrite(light3, LOW);
  digitalWrite(light4, LOW);
  digitalWrite(light5, LOW);
}

void turn2LED(){
  digitalWrite(light1, HIGH);
  digitalWrite(light2, HIGH);
  digitalWrite(light3, LOW);
  digitalWrite(light4, LOW);
  digitalWrite(light5, LOW);
}

void turn3LED(){
  digitalWrite(light1, HIGH);
  digitalWrite(light2, HIGH);
  digitalWrite(light3, HIGH);
  digitalWrite(light4, LOW);
  digitalWrite(light5, LOW);
}

void turn4LED(){
  digitalWrite(light1, HIGH);
  digitalWrite(light2, HIGH);
  digitalWrite(light3, HIGH);
  digitalWrite(light4, HIGH);
  digitalWrite(light5, LOW);
}

void turn5LED(){
  digitalWrite(light1, HIGH);
  digitalWrite(light2, HIGH);
  digitalWrite(light3, HIGH);
  digitalWrite(light4, HIGH);
  digitalWrite(light5, HIGH);
}



