#!/usr/bin/python
import Adafruit_BMP.BMP085 as BMP085

# Instantiate sensor
sensor = BMP085.BMP085()

# Get values from sensor
temperature = format(sensor.read_temperature())
pressure = format(sensor.read_pressure())
altitude = format(sensor.read_altitude())
sealevel_pressure = format(sensor.read_sealevel_pressure())

# Return JSON string
print ("{"  "\"temperature\":" + temperature + ","
			"\"pressure\":" + pressure + ","
			"\"altitude\":" + altitude + "," 
			"\"sealevel_pressure\":" + sealevel_pressure + 
	  "}")