/* Open socket connection with Raspberry Pi IP address */
// var socket = io.connect('http://73.134.193.238:5000');					// IP address for home router
var socket = io.connect('http://172.20.10.11:5000');						// Local IP address for mobile hotspot
var _this = this;

// Initialize temperature variables & HTML elements
var tempDisplay = document.getElementById('temperature-display');
var tempUnits = document.getElementById('temperature-units');
var temp = 0;
var units = 'C';
// Initialize lights variables
var lightIntensity = 0;
// Initialize datetime HTML element
var dtElem = document.getElementById('datetime');
// Request temperature from server
socket.emit('temp');
// Upon temperature response from server
socket.on('temp', function (payload) {
	// Extract temperature from payload object
	temp = payload.temperature;
	// Convert to Fahrenheit if needed
	if (units === 'F') {
		temp = temp * (9/5) + 32;
	}
	// Display temperature on webpage
	tempDisplay.innerHTML = temp.toFixed(2) + " °" + units;
	// Request temperature from server after 2 seconds
	setTimeout(function () { socket.emit('temp') }, 2000);
});
// Upon timestamp response from server
socket.on('datetime', function (payload) {
	// Display timestamp
	dtElem.innerHTML = '<p>' + payload.datetime + '</p>';
})

// Handles button click to swap temperature units
var tempSwapUnits = function () {
	// Convert to °C or °F
	if (units == 'C') {
		temp = temp * (9/5) + 32;
		tempUnits.innerHTML = 'Celsius';
		units = 'F';
	} else {
		temp = (temp - 32) * (5/9);
		tempUnits.innerHTML = 'Fahrenheit';
		units = 'C';
	}
	// Display temperature to webpage
	tempDisplay.innerHTML = temp.toFixed(2) + ' °' + units;
}
// Handles button click to playing music
var playMusic = function () {
	socket.emit('music');
};
// Handles slider change for lights
var onLightsChange = function (value) {
	socket.emit('light', { intensity: value });
};

