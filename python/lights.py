import os
import serial
import sys
from time import sleep

# Get port for serial connection
def getSerialPort():
    return "/dev/"+os.popen("dmesg | egrep ttyACM | cut -f3 -d: | tail -n1").read().strip()

# Open serial connection on baudrate 9600
device = serial.Serial(getSerialPort(), 9600)
# Extract argument sent from app.js
value = sys.argv[1]
# Wait
sleep(2)
# Write value to Arduino via Serial
device.write(value)
# Close serial connection
device.close()
