// Require dependencies
var express = require('express'),
	path = require('path'),
	Gpio = require('onoff').Gpio,
	Raspicam = require('raspicam'),
	PythonShell = require('python-shell');
// Start express app
var app = express();
// Declare port to run server
var port = 5000;
// Link front-end dependencies 
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules/bootstrap/dist')));
app.use(express.static(path.join(__dirname, 'node_modules/socket.io-client')));

// Start server & open socket
var server = app.listen(port);
var io = require('socket.io').listen(server);
console.log('Server running on port ' + port);

/* Handle requests */
// Handle index page request
app.get('/index', function (req, res) {
	console.log('Sending index page');
	// Get time stamp from RTC
	PythonShell.run('python/clock.py', function (err, results) {
		if (err) {
			console.log(err);
			return;
		}
		for (var i = 0; i < results.length; i++) {
			if (results[i].length > 30) {
				console.log(results[i]);
				io.sockets.emit('datetime', { datetime: results[i] });
				return;
			}
		}
		return;
	});

	res.sendFile(path.join(__dirname, 'public', 'index.html'));
});
// Handle live stream page request
app.get('/stream', function (req, res) {
	console.log('Sending stream page');
	res.sendFile(path.join(__dirname, 'public', 'livestream.html'));
});

/* Socket listener */
io.sockets.on('connection', function (socket) {										// Called on new connection to socket

	// Called upon temperature request
	socket.on('temp', function (payload) {
		console.log('Requesting temperature');
		// Run python script to get temperature
		PythonShell.run('python/temperature.py', function (err, results) {
			if (err) {
				console.log(err);
				return;
			}
			results = results[0];													// Extract data
			var data = JSON.parse(results);											// Parse data to JSON
			socket.emit('temp', data);
			console.log('SUCCESS: Temperature retrieved');
		})
	});

	// Called upon sound request
	socket.on('music', function (payload) {
		console.log('Requesting music');
		// Run python script to play sound
		PythonShell.run('python/music.py', function (err, results) {
			if (err) {
				console.log(err);
				return;
			}
			console.log('SUCCESS: Playing music');	
		});
	});

	// Called upon light settings changed
	socket.on('light', function (payload) {
		console.log('Setting lights to %d', payload.intensity);
		// Run python script to set light change
		PythonShell.run('python/lights.py', { args: payload.intensity }, function (err, results) {
			if (err) {
				console.log(err);
				return;
			}
			console.log('SUCCESS: Lights changed');
		});
	});

});

// Initialize camera options
var camera, camOpts = {
	mode: 'photo',
	output: __dirname + '/public/img/image.jpg',
	timeout: 0,
	rotation: 180
};

// Function called to emit live stream  to all sockets
var liveStream = function () {
	camera = new Raspicam(camOpts);
	camera.start();
	camera.on('read', function (err, filename) {
		io.sockets.emit('refresh-stream');
		liveStream();
	});
	return;
};

/* Start live stream */
liveStream();
