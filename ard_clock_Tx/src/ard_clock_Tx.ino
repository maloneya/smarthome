#include <SoftwareSerial.h>  
#include <Wire.h>
#define DS3231_I2C_ADDRESS 0x68

int myTxPin = 3;  // Chosen pin for transmitting data
int myRxPin = 4;  // Chosen pin for receiving data
SoftwareSerial softSerial(myRxPin, myTxPin);

int interrupt_pin = 2;
bool send_time = false;

// =============================CLOCK HELPERS=================================
void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte
dayOfMonth, byte month, byte year){
  // sets time and date data to DS3231
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set next input to start at the seconds register
  Wire.write(decToBcd(second)); // set seconds
  Wire.write(decToBcd(minute)); // set minutes
  Wire.write(decToBcd(hour)); // set hours
  Wire.write(decToBcd(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday)
  Wire.write(decToBcd(dayOfMonth)); // set date (1 to 31)
  Wire.write(decToBcd(month)); // set month
  Wire.write(decToBcd(year)); // set year (0 to 99)
  Wire.endTransmission();
}

/* 
* Convert decimal to binary coded decimal (i.e. 92 => 1001 0010)
* Works for 0 - 99 (decimal)
*/
byte decToBcd(byte val)
{
  return( (val/10 << 4) + (val%10) );
}

byte bcdToDec(byte val)
{
  return(( (val>>4)*10) + (val%16) );
}


void readDS3231time(byte *second, byte *minute, byte *hour, byte *dayOfWeek, byte *dayOfMonth, byte *month, byte *year) {
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set DS3231 register pointer to 00h
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);
  // request seven bytes of data from DS3231 starting from register 00h
  *second = bcdToDec(Wire.read() & 0x7f);
  *minute = bcdToDec(Wire.read());
  *hour = bcdToDec(Wire.read() & 0x3f);
  *dayOfWeek = bcdToDec(Wire.read());
  *dayOfMonth = bcdToDec(Wire.read());
  *month = bcdToDec(Wire.read());
  *year = bcdToDec(Wire.read());
}

// =============================CLOCK MAIN FUNCTION=================================

void displayTime() {
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  // retrieve data from DS3231
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month,
  &year);
  // send it to the serial monitor
   softSerial.print(hour, DEC);
  // // convert the byte variable to a decimal number when displayed
   softSerial.print(":");
  
   if (minute<10) {
     softSerial.print("0");
   }
   softSerial.print(minute, DEC);
   softSerial.print(":");
   if (second<10) {
     Serial.print("0");
   }
  
   softSerial.print(second, DEC);
   softSerial.print(" ");
   softSerial.print(dayOfMonth, DEC);
   softSerial.print("/");
   softSerial.print(month, DEC);
   softSerial.print("/");
   softSerial.print(year, DEC);
  
   softSerial.print(" Day of week: ");
  switch(dayOfWeek){
  case 1:
    softSerial.println("Sunday");
    break;
  case 2:
    softSerial.println("Monday");
    break;
  case 3:
    softSerial.println("Tuesday");
    break;
  case 4:
    softSerial.println("Wednesday");
    break;
  case 5:
    softSerial.println("Thursday");
    break;
  case 6:
    softSerial.println("Friday");
    break;
  case 7:
    softSerial.println("Saturday");
    break;
  }
} 

// =============================ARD MAIN=================================


void setup() {
  Serial.begin(9600);
  softSerial.begin(9600);  // Start the software serial at 9600
  Wire.begin();
  //look into getting correct time from arduino. 
  setDS3231time(00,49,9,7,27,12,14);

  pinMode(interrupt_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interrupt_pin), sendTime, RISING);
  interrupts(); 

}

void loop() {
  if(send_time) {
    displayTime();
    send_time = false;
  }
}

void sendTime() {send_time = true;}
