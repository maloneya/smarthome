import os
import serial
from time import sleep

def getSerialPort():
    return "/dev/"+os.popen("dmesg | egrep ttyACM | cut -f3 -d: | tail -n1").read().strip()

# Get port for serial connection
port = getSerialPort()
# Open serial connection with baudrate 9600
device = serial.Serial(port=port, baudrate=9600)
# Wait
sleep(2)
# Send 'c' to Arduino via serial
device.write('c')
# Read response from serial
s = device.readline()
# If response received, return it
if s:
	print s,
# Close serial connection 
device.close()
