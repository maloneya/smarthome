import os
import serial
from time import sleep

def getSerialPort():
    return "/dev/"+os.popen("dmesg | egrep ttyACM | cut -f3 -d: | tail -n1").read().strip()

# Get port for serial connection
port = getSerialPort()
# Open serial connection with baudrate 9600
device = serial.Serial(port, 9600)
# Wait
sleep(2)
# Write 'm' to Arduino via serial
device.write('m')
# Close serial connection
device.close()
