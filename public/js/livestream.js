/* Open socket connection with Raspberry Pi IP address */
// var socket = io.connect('http://73.134.193.238:5000');					// IP address for home router
var socket = io.connect('http://172.20.10.11:5000');						// Local IP address for mobile hotspot

// Initialize HTML elements
var stream = document.getElementById('stream');
var timestampElem = document.getElementById('timestamp');
var loader = document.getElementById('loader');

// Upon stream response from server
socket.on('refresh-stream', function() {
	// Format timestamp
	var timestamp = new Date().toISOString().
  		replace(/T/, ' ').
  		replace(/\..+/, '');
  	// Display timestamp to webpage
  	timestampElem.innerHTML = timestamp;
  	// Reload live stream image from server
	stream.src = '/img/image.jpg?' + new Date().getTime();
	// Hide CSS loader
	loader.style.display = 'none';
});
